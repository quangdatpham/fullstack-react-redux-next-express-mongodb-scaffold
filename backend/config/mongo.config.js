'use strict';

const { MongoClient } = require('mongodb');

const configs = {
  connectionString: process.env.MONGODB_URL,
  dbName: process.env.DB_NAME,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  attributes: {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 500,
    poolSize: 10,
    bufferMaxEntries: 0,
    connectTimeoutMS: 10000,
    socketTimeoutMS: 45000,
    family: 4
  }
};

const connect = () => new Promise((resolve, reject) => {
  MongoClient.connect(
    configs.connectionString,
    configs.attributes,
    (err, client) => {
      if (err)
        return reject(err);

      const db = client.db(configs.dbName);
      resolve(db);
    }
  )
})

module.exports = Object.create({ connect });
