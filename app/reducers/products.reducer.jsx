import { ADD_PRODUCT, DELETE_PRODUCT } from '../actions/products.action';

export default (state = [], action) => {
  switch (action.type) {
    case ADD_PRODUCT:
      const { _id, name, price } = action;

      return [
        ...state,
        {
          _id, name, price
        }
      ];
    case DELETE_PRODUCT:
      const { _id: _idToDelete } = action;

      return state.filter(({ _id}) => _id !== _idToDelete);
    default:
      return state;
  }
}