import React from 'react';
import App from 'next/app';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl';
import ApolloClient from 'apollo-client';
import { ApolloProvider, useQuery } from '@apollo/react-hooks';
import fetch from 'node-fetch';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory'

import '../styles/styles.scss';

import store from '../store';
import messages from '../i18n';

const client = new ApolloClient({
  link: createHttpLink({
    uri: 'http://localhost:3000/graphql',
    fetch
  }),
  cache: new InMemoryCache()
});

export default class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    return {
      pageProps: Component.getInitialProps
        ? await Component.getInitialProps(ctx)
        : {}
    };
  }

  constructor(props) {
    super(props);
  }

  render() {
    const { Component, pageProps } = this.props;

    const locale = store.getState().app.locale || 'en-US';
    const [, message] = Object.entries(messages).find(([l,]) => locale.toLocaleLowerCase().includes(l));

    return (
      <Provider store={store}>
        <IntlProvider {...{ locale, messages: message }}>
          <ApolloProvider client={client}>
            <Component {...pageProps} />
          </ApolloProvider>
        </IntlProvider>
      </Provider>
    );
  }
}