const { authMiddleware: auth } = require('./backend/middlewares/');

module.exports = app => {
  app.get([ '/app', '/home' ], (req, res) => res.redirect('/'));

  app.get([ '/about', '/author' ], (req, res) => res.redirect('/api/author'));
}