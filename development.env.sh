# server
export PORT="3000"
export SSL=""

# database
export MONGODB_URL="mongodb://localhost:27017"
export DB_NAME="time-for-lunch"
export DB_USER="root"
export DB_PASS="root"

# jwt
export JWT_SECRET="FDJW035897BCNSAPTEIUC89B6AT2Q1Q6VCJAFJD2KANVN3BA72PRUE0QKA"
export JWT_EXPIRES=6000000

export COOKIE_EXP=6000000